//
//  main.cpp
//  OS LR-3
//
//  Created by Дмитрий Пархоменко on 06.04.2018.
//  Copyright © 2018 Дмитрий Пархоменко. All rights reserved.
//

#include <iostream>
#include <cstdlib>
#pragma intrinsic(__rdtsc)using namespace std;

using namespace std;
#define RESET   "\033[0m"
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */

const string color[6] = {
    "\033[32m","\033[36m","\033[34m","\033[35m", "\033[31m","\033[33m",
};
const int colorSize=6;


unsigned long long rdtsc() {    //возвращает текущий такт процессора
    return __rdtsc();
}

class Matrix {
    int **massIn;
    int **massSequence;
    int **diagram;
    int absMaxDiagramIndex=0;  //максимально возможное время обработки (для диаграммы)
    unsigned long long startTact, finishTact;
    
    int rows, colls=3;
    int massSample[5][3] = {
        {2,3,4},
        {1,1,5},
        {4,2,5},
        {5,3,7},
        {3,4,6}
    };
    
public:
    Matrix() {
        massIn = new int*[rows];
        for (int i=0; i<rows; i++) {
            massIn[i] = new int[colls];
        }
        massSequence = new int*[rows];
        for (int i=0; i<rows; i++) {
            massSequence[i] = new int[colls];
        }

    }
    /*~Matrix() {
        for (int i=0; i<rows; i++) {
            delete[] massIn[i];
        }
        delete [] massIn;
        
        for (int i=0; i<rows; i++) {
            delete[] massSequence[i];
        }
        delete [] massSequence;
        
        for (int i=0; i<colls; i++) {
            delete[] diagram[i];
        }
        delete [] diagram;
        
        cout << endl << "Временные матрицы удалены\n";
    }*/
    void massInOut() {
        cout << "Матрица трудоемкости: " << endl;
        for (int i=0; i<rows; i++) {
            for (int j=0; j<colls; j++) {
                cout << massIn[i][j] << "\t";
            }
            cout << endl;
        }
        cout << endl;
    }
    void massSequenceOut() {
        int currentColorIndex=0;
        cout << "Последовательность заполнения:" << endl;
        for (int i=0; i<rows; i++) {
            cout << color[currentColorIndex] << massSequence[i][0]+1 << " ";
            if (currentColorIndex==colorSize) currentColorIndex = 0;
            else currentColorIndex++;
            //for (int j=0; j<colls; j++) {
            //    if (j==0) cout << massSequence[i][j]+1 << "\t";
            //    else cout << massSequence[i][j] << "\t";
            //}
            //cout << endl;
        }
        cout << endl << endl;
    }
    
    void massInSampleFill(){
        rows = 5;
        for (int i=0; i<rows; i++) {
            for (int j=0; j<colls; j++) {
                massIn[i][j]=massSample[i][j];
            }
        }
    }
    
    void massInManualFill() {
        cout << "Введите количество строк: ";
        cin >> rows;
        for (int i=0; i<rows; i++) {
            cout<<"Строка "<<i+1<<endl;
            for (int j=0; j<colls; j++) {
                cout << "Столбец "<<j+1<<":\t";
                cin >> massIn[i][j];
            }
            cout << endl;
        }
    }
    
    void massInRandomFill() {
        cout << "Введите количество строк: ";
        cin >> rows;
        cout << "Введите максимальное число: ";
        int max;
        cin >> max;
        for (int i=0; i<rows; i++) {
            for (int j=0; j<colls; j++) {
                massIn[i][j] = 1+rand()%max;
            }
        }
    }
    
    void massSequenceFill(){
        for (int i=0; i<rows; i++) massSequence[i][0] = i;    //первый столбец с номерами job's
        for (int i=0; i<rows; i++) {    //вычисление суммировпной таблицы
            for (int j=0; j<colls; j++) {
                massSequence[i][j+1] = massIn[i][j]+massIn[i][j+1];
            }
        }
        
        int** tempMass = new int*[rows];  //временный массив с упорядочеными
        for (int i=0; i<rows; i++) {
            tempMass[i] = new int[colls];
        }
        int tempMassCurrentIndex = 0;
        
        startTact = rdtsc();
        
        //сортировка по столбцу
        int minValIndex = 0;
        int *tempStr = new int[3] {massSequence[0][0], massSequence[0][1], massSequence[0][2]};
        for (int i=0; i<rows; i++) {
            for (int j=0; j<rows; j++) {
                if (massSequence[j][1] < tempStr[1]) {
                    minValIndex = j;
                    tempStr[1] = massSequence[j][1];
                }
            }
            for (int k=0; k<3; k++) tempStr[k]=massSequence[minValIndex][k];
            massSequence[minValIndex][1] = 99999;
            
            for (int j=0; j<3; j++) tempMass[tempMassCurrentIndex][j] = tempStr[j];
            tempMassCurrentIndex++;
            tempStr[1]=99999;
        }
        
        //обратное копирование в "отсортированный"
        for (int i=0; i<rows; i++) for (int j=0; j<colls; j++) massSequence[i][j]=tempMass[i][j];
        
        for (int i=0; i<rows; i++) {
            delete[] tempMass[i];
        }
        delete [] tempMass;
    }
    
    void diagramFill() {
        //вычисление памяти с запасом под массив - максимальное возможное время обработки
        for (int i=0; i<rows; i++) {        //проход по строкам отсортированного массива
            for (int j=0; j<colls; j++) {   //проход по столбцам певого массива и вывод диаграмм
                for (int k=0; k<massIn[massSequence[i][0]][j]; k++)  absMaxDiagramIndex++;
            }
        }
        
        //создание массива диаграммы
        diagram = new int*[colls];
        for (int i=0; i<colls; i++) {
            diagram[i] = new int[absMaxDiagramIndex];
        }
        for (int i=0; i<colls; i++) {
            for (int j=0; j<absMaxDiagramIndex; j++) {
                diagram[i][j]=0;
            }
        }
        
        
        int diagramMinIntex[3]={0}; //стартовая точка для элемента в массиве диаграммы
        
        //заполнение массива диаграммы
        for (int i=0; i<rows; i++) {        //проход по строкам отсортированного массива
            for (int j=0; j<colls; j++) {   //проход по строкам диаграммы
                int tempMinIndex = 0;
                do {    //вывод несколько раз одних и тех же элементов в диаграмму (бред, но по другому не работает(for не хочет проходить по массиву))
                    if (tempMinIndex == 0) tempMinIndex = diagramMinIntex[j];
                    diagram[j][tempMinIndex] = massIn[massSequence[i][0]][1]+1;
                    tempMinIndex++;
                } while(tempMinIndex!=(massIn[massSequence[i][0]][j]+diagramMinIntex[j]));
                
                
                //добавление с какого числа начинается ввод следующего числа
                //diagramMinIntex[j] += massIn[massSequence[i][0]][j];
                if (j==0) {
                    diagramMinIntex[j] += massIn[massSequence[i][0]][j];
                    if (i==0) {
                        diagramMinIntex[j+1] += massIn[massSequence[i][0]][j];
                        diagramMinIntex[j+2] += massIn[massSequence[i][0]][j];
                    }
                    //замена следующего элемента
                    if (diagramMinIntex[j+1]+massIn[massSequence[i][0]][j+1] > diagramMinIntex[j]+massIn[massSequence[i][0]][j+1]) {
                        diagramMinIntex[j+1] += massIn[massSequence[i][0]][j+1];
                    } else diagramMinIntex[j+1] = diagramMinIntex[j];
                }
                
                if (j==1) {
                    if (i==0) diagramMinIntex[j+1] += massIn[massSequence[i][0]][j];
                }
                
                if (j==2) {
                    if ((diagramMinIntex[j] + massIn[massSequence[i][0]][j])   > (diagramMinIntex[j-1] + massIn[massSequence[i][0]][j-1])) {
                       diagramMinIntex[j] = diagramMinIntex[j] + massIn[massSequence[i][0]][j];
                    } else diagramMinIntex[j] = diagramMinIntex[j-1];
                }
            }
        }
        
        finishTact = rdtsc()-startTact;
        
        //обрезка массива диаграммы
        int tempDiagramIndex = absMaxDiagramIndex;
        for (int j=absMaxDiagramIndex; j>0; j--) {
            if (diagram[2][j]!=0) {tempDiagramIndex++; break;}
            else tempDiagramIndex--;
        }
        
        int ** tempDiagram = new int*[colls];
        for (int i=0; i<colls; i++) tempDiagram[i] = new int[tempDiagramIndex];
        for (int i=0; i<colls; i++) for (int j=0; j<tempDiagramIndex; j++) tempDiagram[i][j] = diagram[i][j];
        for (int i=0; i<colls; i++) delete[] diagram[i];
        
        absMaxDiagramIndex = tempDiagramIndex;
        for (int i=0; i<colls; i++) diagram[i] = new int[absMaxDiagramIndex];
        for (int i=0; i<colls; i++) for (int j=0; j<tempDiagramIndex; j++) diagram[i][j] = tempDiagram[i][j];
        for (int i=0; i<colls; i++) delete [] tempDiagram[i];
        delete [] tempDiagram;
    }
    
    void diagramOut() {
        int currentColorIndex;
        cout << "Диаграмма: " << endl;
        for (int i=0; i<colls; i++){
            currentColorIndex = 0;
            for (int j=0; j<absMaxDiagramIndex; j++) {
                if (diagram[i][j] == 0) {
                    cout << diagram[i][j] << " ";
                } else {
                    cout << color[currentColorIndex] << diagram[i][j] << RESET << " ";
                    if (diagram[i][j]!=diagram[i][j+1]) {
                        if (currentColorIndex == colorSize) currentColorIndex=0;
                        else currentColorIndex++;
                    }
                }
            }
            cout << endl << endl;
        }
    }
    void tactOut() {
        cout << "Количество тактов процессора: " << finishTact << RESET << endl;
    }
    
    bool checkMass() {
        int minY1=massIn[0][0], maxY2=massIn[0][1], minY3=massIn[0][2];
        for (int i=0; i<rows; i++) {
            if (massIn[i][0]<minY1) minY1=massIn[i][0];
            if (massIn[i][1]>maxY2) maxY2=massIn[i][1];
            if (massIn[i][2]<minY1) minY3=massIn[i][2];
        }
        
        cout << "(minY1 = " << minY1 << ") ";
        if (minY1>=maxY2) cout << ">= ";
        else cout << "<= ";
        cout << "(maxY2 = " << maxY2 << ")" << endl;
        cout << "(minY3 = " << minY3 << ") ";
        if (minY3 >= maxY2) cout << ">= ";
        else cout << "<= ";
        cout << "(maxY2 = " << maxY2 << ")" << endl;
        
        if (minY3>=maxY2 || minY1>=maxY2) {
            cout << CYAN << "Условие выполняется" << RESET << endl << endl;
            return true;
        }
        else {
            cout << MAGENTA << "Условие не выполняется" << RESET << endl << endl;
            return false;
        }
    }
};


int main() {
    int n = 0;
    do {
        cout << "0.Выход\n1.Ручной ввод матрицы\n2.Ввод из заготовки\n3.Заполнить случайными числами\n";
        cin >> n;
        
        if (n!=0){
            Matrix m;
            if (n==1) m.massInManualFill();  //ручной ввод
            if (n==2) m.massInSampleFill();   //ввод из заготовки
            if (n==3) m.massInRandomFill();   //заполенение случайными числами
            m.massInOut();
            
            if (m.checkMass()) {
                m.massSequenceFill();
                m.diagramFill();
                
                m.massSequenceOut();
                m.diagramOut();
                m.tactOut();
            }
        }
    }while (n!=0);
    return 0;
}

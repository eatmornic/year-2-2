﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library {
    public class Book {
        public Book(int id) {
            this.id = id;
            Console.Write("Введите название книги:");
            title = Console.ReadLine().ToString();
            Console.Write("Введите год издания:");
            year = int.Parse(Console.ReadLine());
            Console.Write("Введите описание книги:");
            description = Console.ReadLine().ToString();
        }
        public Library Library { get; set; }



        private int id = 0;

        private Reader reader;

        private string title = "null";
        private int year = 0;
        private bool status = true;
        private string description = "null";

        public void Out() {
            Console.ForegroundColor = ConsoleColor.Cyan;
            if (status) {
                Console.WriteLine("\tID:" + id +
                                  " Title:" + title +
                                  " Year:" + year +
                                  " Status:" + status +
                                  " Descr:" + description);
            }
            Console.ResetColor();
        }

        public void OutIssued() {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            if (!status) {
                Console.Write("\tID:" + id +
                              " Title:" + title +
                              " Year:" + year +
                              " Status:" + status + " ");
                reader.Show();
            }
            Console.ResetColor();
        }

        public void OutIssued(Reader reader){
            if (this.reader == reader) {
                OutIssued();
            }
        }

        public void OutAll() {
            Out();
            OutIssued();
        }

        public void Take(Reader reader) {
            status = false;
            this.reader = reader;
        }
        public void Return() {
            status = true;
        }
    }
}
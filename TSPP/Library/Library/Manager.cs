﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library {
    public class Manager {
        public Manager() {
        }
        private string name;

        public Library addBook(Library library)  {
            library.books.Add(new Book(library.books.Count + 1));
            return library;
        }

        public void SeeAllCatalog(Library library) {
            library.SeeAllCatalog();
        }

        public List<Reader> AddReader(List<Reader> readers) {
            readers.Add(new Reader(readers.Count + 1));
            return readers;
        }

        public List<Reader> AddPrivReader(List<Reader> readers) {
            readers.Add(new PrivilegedReader(readers.Count + 1));
            return readers;
        }
    }
}

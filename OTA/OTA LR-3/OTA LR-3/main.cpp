//  main.cpp
//  OTA LR-3
//
//  Created by Дмитрий Пархоменко on 12.03.2018.
//  Copyright © 2018 Дмитрий Пархоменко. All rights reserved.
//
//  Создать массив, заполнить случайными числами, пройтись по массиву, складывая числа, если сумма = заданному числу V, то остановить алгоритм или продолжать до конца. Вывести ХЗ что.
//  Посчитатать струдоемкость алгоритма. ХЗ как

#include <iostream>
#include <cmath>
using namespace std;

class Mass {
    int size, max, v;
    
};

//Заполенение массива случайными числами
void fillRandMass(int *size, int *max, int *mass) {
    srand(time(NULL));
    for (int i = 0; i<*size; i++) {
        mass[i] = 1+rand()%*max;
    }
}

void fillMass(int *size, int *mass) {
    for (int i=0; i<*size; i++) {
        cin >> mass[i];
    }
}
void outMass(int *size, int *mass){
    cout << "Масссив чисел:\n";
    for (int i=0; i<*size; i++) {
        cout << mass[i] << "\t";
    }
    cout << endl;
}



void taskSumm(int *size, int *mass, int *v) {
    int summ=0;
    int complexity = 0;
    int stop=NULL;
    for (int i=0; i<*size; i++) {
        summ+=mass[i];
        complexity++;
        if (summ >= *v) {
            cout << summ << endl;
            stop = i;
            complexity++;
            break;
        }
    }
    complexity = (complexity+=(1+3*stop));
    if (stop==NULL) cout << "Сумма меньше V" << endl;
    cout << "Количество элементарных операций: " << complexity << endl;
}



int main() {
    int size = 11;
    int max = 80;
    int v = 15;
    int *mass = new int[size];
    
    //fillRandMass(&size, &max, mass);
    fillMass(&size, mass);
    outMass(&size, mass);
    
    taskSumm(&size, mass, &v);
    
    delete [] mass;
    return 0;
}

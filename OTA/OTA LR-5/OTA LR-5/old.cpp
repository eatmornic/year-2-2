//
//  main.cpp
//  OTA LR-5
//
//  Created by Дмитрий Пархоменко on 05.04.2018.
//  Copyright © 2018 Дмитрий Пархоменко. All rights reserved.
//
//  ПОИСК КРАТЧАЙШИХ ПУТЕЙ НА ГРАФАХ

#include <iostream>
#include <cstdlib>
#pragma intrinsic(__rdtsc)using namespace std;
using namespace std;

unsigned long long rdtsc() {    //возвращает текущий такт процессора
    return __rdtsc();
}

class Matrix {
    unsigned long long startTact, finishTact;
    int massSample[8][9] = {
        {0,2,1,0,0,0,0,0,0},
        {0,0,7,2,0,0,0,0,0},
        {0,0,0,0,2,3,0,6,0},
        {0,0,3,0,0,0,8,0,0},
        {0,0,0,0,0,0,4,0,0},
        {0,0,0,0,3,0,0,0,0},
        {0,0,0,0,0,2,0,0,10},
        {0,0,0,0,0,0,0,0,1}
    };
    
    int **massIn;   // начальный массив
    
public:
    Matrix() {
        
    }
    ~Matrix() {
        
    }
    
    void massInFillFromSample() {
        massIn = new int*[8];
        for (int i=0; i<8; i++) massIn[i] = new int[9];
    }
};


int main() {
    
    Matrix m;
    
    
    
    return 0;
}
